<?php

namespace TestApi\Entity\Core;

use TestApi\Helpers\CamelCaseHelper;

abstract class CoreEntity
{
    private $tableName;

    public function __construct($name = self::class, $data = [])
    {
        $this->setTableName($name);
        if($data)
        {
            foreach ($data as $k => $v)
            {
                if(method_exists($this, 'set'.CamelCaseHelper::underscoreToCamel($k, true)))
                {
                    $method = $k === 'id' ? 'setTestApiId' : 'set'.CamelCaseHelper::underscoreToCamel($k, true);
                    $this->$method($v);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    public function setTableName($name)
    {
        preg_match_all('|(\w*)$|', $name, $matches);
        $this->tableName = CamelCaseHelper::camelToUnderscore($matches[0][0]).'s';
    }
}