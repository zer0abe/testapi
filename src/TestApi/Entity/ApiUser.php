<?php

namespace TestApi\Entity;

use TestApi\Entity\Core\CoreEntity;
use TestApi\Helpers\CamelCaseHelper;
use TestApi\Helpers\CliHelper;

class ApiUser extends CoreEntity
{
    /**
     * @var int database index of user
     */
    private $id;

    /**
     * @var string user email
     */
    private $email;

    /**
     * @var string user password
     */
    private $plainPassword;

    /**
     * @var string Test API token
     */
    private $token;

    /**
     * @var \DateTime
     */
    private $tokenBirthDate;

    /**
     * @var \DateTime object with last users successful request
     */
    private $lastRequestDate;

    /**
     * @var bool
     */
    private $lastRequestSuccess;

    /**
     * TestApiUser constructor.
     * @param null|\PDOStatement $data
     * @internal param string $email
     * @internal param string $plainPassword
     */
    public function __construct($data = null)
    {
        $this->setTableName(self::class);
        if($data)
        {
            foreach ($data as $k => $v)
            {
                if(method_exists($this, 'set'.CamelCaseHelper::underscoreToCamel($k, true)))
                {
                    $method = 'set'.CamelCaseHelper::underscoreToCamel($k, true);
                    $this->$method($v);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @return \DateTime
     */
    public function getLastRequestDate()
    {
        if(!$this->lastRequestDate)
            return null;

        return $this->lastRequestDate->format('Y-m-d H:i:s');
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param \DateTime $lastRequestDate
     */
    public function setLastRequestDate($lastRequestDate)
    {
        if(!$lastRequestDate instanceof \DateTime)
            $this->lastRequestDate = \DateTime::createFromFormat("Y-m-d H:i:s", $lastRequestDate);
        else
            $this->lastRequestDate = $lastRequestDate;
    }

    /**
     * @param string $plainPassword
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function getTokenBirthDate()
    {
        if(!$this->tokenBirthDate)
            return null;
        return $this->tokenBirthDate->format('Y-m-d H:i:s');
    }
    /**
     * @param \DateTime $tokenBirthDate
     */
    public function setTokenBirthDate($tokenBirthDate)
    {
        if(!$tokenBirthDate instanceof \DateTime)
            $this->tokenBirthDate = \DateTime::createFromFormat('Y-m-d H:i:s', $tokenBirthDate);
        else
            $this->tokenBirthDate = $tokenBirthDate;
    }

    /**
     * @return bool|\DateInterval
     */
    public function checkTokenLifeTime()
    {
        if(!$this->getToken())
        {
            return false;
        }
        $now = new \DateTime();
        $tokenBirthDate = \DateTime::createFromFormat('Y-m-d H:i:s', $this->getTokenBirthDate());
        $diff = $tokenBirthDate->diff($now);
        return $diff;
    }

    /**
     * @return boolean
     */
    public function getLastRequestSuccess()
    {
        return $this->lastRequestSuccess;
    }

    /**
     * @param $lastRequestSuccess
     **/
    public function setLastRequestSuccess($lastRequestSuccess)
    {
        $this->lastRequestSuccess = $lastRequestSuccess;
    }
}