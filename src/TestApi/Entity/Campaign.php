<?php
namespace TestApi\Entity;


use TestApi\Entity\Core\CoreEntity;

class Campaign extends CoreEntity
{
    protected $id;

    protected $name;

    protected $balance;

    protected $userId;

    /**
     * @var int
     */
    protected $testApiId;

    public function __construct(array $data)
    {
        parent::__construct(self::class, $data);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getBalance()
    {
        return (string) $this->balance / 100;
    }

    /**
     * @param string $balance
     */
    public function setBalance($balance)
    {
        $this->balance = (int) (((float)$balance) * 100);
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getTestApiId()
    {
        return $this->testApiId;
    }

    /**
     * @param mixed $testApiId
     */
    public function setTestApiId($testApiId)
    {
        $this->testApiId = $testApiId;
    }
}