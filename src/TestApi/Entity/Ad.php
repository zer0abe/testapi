<?php
namespace TestApi\Entity;


use TestApi\Entity\Core\CoreEntity;

class Ad extends CoreEntity
{
    private $id;

    private $adGroupId;

    private $campaignId;

    private $title;

    private $text;

    private $href;

    private $testApiId;

    public function __construct(array $data)
    {
        parent::__construct(self::class, $data);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->adGroupId;
    }

    /**
     * @param mixed $adGroupId
     */
    public function setAdGroupId($adGroupId)
    {
        $this->adGroupId = $adGroupId;
    }

    /**
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * @param mixed $campaignId
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     */
    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * @return mixed
     */
    public function getTestApiId()
    {
        return $this->testApiId;
    }

    /**
     * @param mixed $testApiId
     */
    public function setTestApiId($testApiId)
    {
        $this->testApiId = $testApiId;
    }
}