<?php
namespace TestApi\Entity;

use TestApi\Entity\Core\CoreEntity;

class Group extends CoreEntity
{
    private $id;

    private $campaignId;

    private $name;

    private $testApiId;

    public function __construct(array $data)
    {
        parent::__construct(self::class, $data);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * @param mixed $campaignId
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTestApiId()
    {
        return $this->testApiId;
    }

    /**
     * @param mixed $testApiId
     */
    public function setTestApiId($testApiId)
    {
        $this->testApiId = $testApiId;
    }


}