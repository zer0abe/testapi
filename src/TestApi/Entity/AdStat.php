<?php
namespace TestApi\Entity;


use TestApi\Entity\Core\CoreEntity;

class AdStat extends CoreEntity
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $adId;

    /**
     * @var int
     */
    private $phraseId;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var int
     */
    private $clicks;

    /**
     * @var int
     */
    private $shows;

    /**
     * @var int
     */
    private $amount;

    /**
     * @var int
     */
    private $ctr;

    public function __construct(array $data)
    {
        parent::__construct(self::class, $data);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAdId()
    {
        return $this->adId;
    }

    /**
     * @param int $adId
     */
    public function setAdId($adId)
    {
        $this->adId = $adId;
    }

    /**
     * @return int
     */
    public function getPhraseId()
    {
        return $this->phraseId;
    }

    /**
     * @param int $phraseId
     */
    public function setPhraseId($phraseId)
    {
        $this->phraseId = $phraseId;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date->format("Y-m-d");
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        if(!$date instanceof \DateTime)
            $this->date = \DateTime::createFromFormat("Y-m-d", $date);
        else
            $this->date = $date;
    }

    /**
     * @return int
     */
    public function getClicks()
    {
        return $this->clicks;
    }

    /**
     * @param int $clicks
     */
    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
    }

    /**
     * @return int
     */
    public function getShows()
    {
        return $this->shows;
    }

    /**
     * @param int $shows
     */
    public function setShows($shows)
    {
        $this->shows = $shows;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount/100;
    }

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount * 100;
    }

    /**
     * @return int
     */
    public function getCtr()
    {
        return $this->ctr/10000;
    }

    /**
     * @param int $ctr
     */
    public function setCtr($ctr)
    {
        $this->ctr = $ctr * 10000;
    }
}