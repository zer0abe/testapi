<?php
namespace TestApi\Entity;


use TestApi\Entity\Core\CoreEntity;

class Phrase extends CoreEntity
{
    private $id;

    private $adId;

    private $adGroupId;

    private $campaignId;

    private $phrase;

    private $testApiId;

    public function __construct(array $data)
    {
        parent::__construct(self::class, $data);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAdId()
    {
        return $this->adId;
    }

    /**
     * @param mixed $adId
     */
    public function setAdId($adId)
    {
        $this->adId = $adId;
    }

    /**
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->adGroupId;
    }

    /**
     * @param mixed $adGroupId
     */
    public function setAdGroupId($adGroupId)
    {
        $this->adGroupId = $adGroupId;
    }

    /**
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaignId;
    }

    /**
     * @param mixed $campaignId
     */
    public function setCampaignId($campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @return mixed
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @param mixed $phrase
     */
    public function setPhrase($phrase)
    {
        $this->phrase = $phrase;
    }

    /**
     * @return mixed
     */
    public function getTestApiId()
    {
        return $this->testApiId;
    }

    /**
     * @param $testApiId
     *
     */
    public function setTestApiId($testApiId)
    {
        $this->testApiId = $testApiId;
    }
}