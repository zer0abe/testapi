<?php

namespace TestApi\Helpers;


use TestApi\Entity\AdStat;
use TestApi\Entity\ApiUser;
use TestApi\Entity\PhraseStat;
use TestApi\Entity\Core\CoreEntity;
use TestApi\PdoWrapper;
use TestApi\Helpers\CamelCaseHelper;
use TestApi\Helpers\CliHelper;

class DatabaseHelper
{
    public static function fetchUser($email, $password)
    {
            $sql = "
                    SELECT * FROM `api_users` WHERE email = ? AND plain_password = ?;
            ";
            $result = PdoWrapper::run($sql, [$email, $password])->fetch();
            if ($result) {
                CliHelper::outln("User with email " . $email . " exists.");
                $user = new ApiUser($result);
            }
            else
            {
                $user = new ApiUser();
                $user->setEmail($email);
                $user->setPlainPassword($password);
                CliHelper::outln("Creating new user with email ". $email);
            }
        return $user;
    }

    public static function checkDatabaseSchema($dbname)
    {
        $sql = "
                SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = ?; 
        ";
        try {
            if (PdoWrapper::run($sql, [$dbname])->rowCount()) {
                $sql = "SELECT COUNT(*) from information_schema.tables 
                        WHERE table_type = 'base table'";
                $count = PdoWrapper::run($sql)->rowCount();
                return $count;
            }
        }
        catch (\PDOException $e)
        {
            return $e;
        }
    }

    public static function prepareScheme($source)
    {
        try
        {
            $stmt = PdoWrapper::run($source);
            return $stmt->rowCount();
        }
        catch (\PDOException $e)
        {
            return $e;
        }
    }

    public static function flushEntity(CoreEntity &$entity)
    {
        $tableName = $entity->getTableName();
        $entityExists = self::entityExists($entity);
        $command = $entityExists ? "UPDATE `" : "INSERT INTO `";
        $dataToFlush = [];
        $allowed = [];
        $values = [];
        $pattern = '/(?<=get)([\w]*)$/';
        foreach (get_class_methods($entity) as $method) {
            if (preg_match($pattern, $method, $matches)) {
                if ($matches[0] != 'TableName' && $matches[0] != 'Id') {
                    $name = CamelCaseHelper::camelToUnderscore(lcfirst($matches[0]));
                    $allowed[] = $name;
                    $dataToFlush[$name] = $entity->$method();
                }
            }
        }
        $pdoSet = PdoWrapper::pdoSet($allowed, $values, $dataToFlush);
        $qString = $command . $tableName . "`";
        $qString .= " SET " . $pdoSet . " ";
        $id = $entity->getId();
        if ($entityExists) {
            if(!$entity instanceof ApiUser) {
                $id = $entity->getTestApiId();
                $qString .= " WHERE `test_api_id` = " . $id;
            }
            else
            {
                $qString .= " WHERE `id` = " . $id;
            }

        }
        $stmt = PdoWrapper::run($qString, $values);
        if(!$entity->getId())
        {
            $entity->setId(PdoWrapper::run("SELECT LAST_INSERT_ID()")->fetch(\PDO::FETCH_NUM)[0]);
        }
        return $stmt->rowCount();
    }

    public static function fetchEntity(CoreEntity &$entity)
    {
        if(self::entityExists($entity)) {
            $tableName = $entity->getTableName();
            $qString = "SELECT * FROM " . $tableName ." WHERE `id`=?";
            $class = get_class($entity);
            $entity = new $class(PdoWrapper::run($qString, [$entity->getId()])->fetch());
            return $entity;
        }
        return null;
    }

    public static function entityExists(CoreEntity $entity)
    {
        $tableName = $entity->getTableName();
        if($entity instanceof ApiUser)
        {
            $qString = "SELECT COUNT(id) as `count` FROM `". $tableName . "` WHERE `id` = ?";
            $id = $entity->getId();
        }
        elseif ($entity instanceof AdStat || $entity instanceof PhraseStat)
        {
            return false;
        }
        else
        {
            $qString = "SELECT COUNT(id) as `count` FROM `". $tableName . "` WHERE `test_api_id` = ?";
            $id = $entity->getTestApiId();
        }
        $result = PdoWrapper::run($qString, [$id])->fetch()['count'];
        return $result;
    }
}