<?php
namespace TestApi\Helpers;

use Aura\Cli\Exception;
use Buzz\Client\Curl;
use Buzz\Message\Form\FormRequest;
use Buzz\Message\Request;
use TestApi\PdoWrapper;
use TestApi\TestApiResponse as Response;
use TestApi\Helpers\JSONHelper;
use TestApi\Entity\ApiUser;
use TestApi\Entity\Phrase;
use TestApi\Entity\AdStat;
use TestApi\Entity\PhraseStat;
use TestApi\Helpers\CliHelper;
use TestApi\TestApiWorker;

class RequestHelper
{
    //Maximum number of requested rows
    const MAX_DATA_LENGTH = 10;

    const MAX_REQUESTS_NUMBER = 10;

    //Delay after maximum possible requests in seconds
    const MAX_REQUEST_INTERVAL = 60;

    /**
     * @var int Number of received rows
     */
    private static $dataCount;

    /**
     * @var \DateTime
     */
    private static $requestDate;

    private static $requestCount;

    /**
     * @param TestApiWorker $worker
     * @param ApiUser       $user
     * @param               $url
     * @param string        $method
     * @param array         $options
     *
     * @return bool|Response
     */
    public static function makeRequest(TestApiWorker &$worker, ApiUser $user, $url, $method = 'POST', $options = [])
    {
        CliHelper::outln("Count of requests " . self::$requestCount);

        $client = new Curl();
        $request = new FormRequest();
        $response = new Response();
        $request->setMethod($method);
        $request->setHost($url);
        if ($options) {
            $request->addFields($options);
        }
        if ($user->checkTokenLifeTime()) {
            $request->setHeaders(['Authorization' => 'Bearer ' . $user->getToken()]);
        }

        try {
            if (self::getRequestCount() == self::MAX_REQUESTS_NUMBER) {
                $delay = self::getRequestDelay();
                CliHelper::outln("Waiting for " . $delay . " seconds to proceed.");
                self::$requestCount = 0;
                sleep($delay);
            }
            $client->send($request, $response);
            self::incrementRequestCount();
            if (self::getRequestCount() == 1)
                self::setLastRequestDate(new \DateTime());
            if ($response->isOk()) {
                $responseBody = $response->getContent();
                $encodedBody = JSONHelper::decode($responseBody);
                return $encodedBody;
            }
            if ($response->getStatusCode() == 401) {
                CliHelper::outln("Request status code: ", $response->getStatusCode());
                $worker->refreshToken($user);
                return self::makeRequest($worker, $user, $url, $method, $options);
            }
            if ($response->getStatusCode() == 404) {
                CliHelper::outln("Request status code: ", $response->getStatusCode());
                return JSONHelper::decode($response->getContent());
            }
            CliHelper::outln($response->getContent());
            return false;
        } catch (\Exception $e) {
            CliHelper::errln("Request refused with error: " . PHP_EOL
                . $e->getMessage() . PHP_EOL . " in file " . $e->getFile() . " on line " . $e->getLine());
            self::incrementRequestCount();
            if (!$response = self::makeRequest($worker, $user, $url, $method, $options))
                die();
            return $response;
        }
    }

    /**
     * @param TestApiWorker $worker
     * @param ApiUser       $user
     * @param               $url
     * @param string        $method
     * @param int           $limit
     * @param bool          $flush      optional bool parameter determines saving and unset entity
     * @param string        $className  optional string parameter providing name of the class that
     *                                  you wish to save in progress
     *
     * @return array | int
     */
    public static function repeatedRequest(TestApiWorker $worker, ApiUser $user, $url, $method = 'POST', $limit = 10, $flush = false, $className = '')
    {
        self::$requestCount = 0;
        $options = [
            'offset' => 0,
            'limit' => $limit
        ];
        $result = [];
        $totalCount = 0;
        $keyList = [];
        do {
            $middleResult = [];
            $data = self::makeRequest($worker, $user, $url, $method, $options);
            $options['offset'] += $limit;
            if (isset($data->data) && count($data->data) && isset($data->total) && $data->total) {
                $objects = $data->data;
                $total = $data->total;
            } elseif ($data->status_code == 404) {
                break;
            } else {
                CliHelper::errln("Wrong response data format, try again later.");
                die();
            }
            if ($objects) {
                $listStringArray = [];
                foreach ($objects as $k => $data) {

                    $data->user_id = $user->getId();
                    if (isset($data->id)) {
                        $data->test_api_id = $data->id;
                        $listStringArray[] = $data->test_api_id;
                        unset($data->id);
                    }
                    if ($flush && $className) {

                        $entity = new $className((array)$data);
                        DatabaseHelper::flushEntity($entity);
                        unset($objects[$k]);
                        unset($data);
                    } else
                        array_push($middleResult, $data);
                    $totalCount++;
                }
                $result = array_merge($result, $middleResult);
                if (count($listStringArray)) {
                    $keyList = array_merge($keyList, $listStringArray);
                }
            }
        } while ($totalCount != $total);
        CliHelper::outln("Counting results: " . $totalCount);
        if (!$flush)
            return $result;
        $lists = array_chunk($keyList, 10);
        foreach ($lists as $k => $list) {
            $lists[$k] = implode(';', $lists[$k]);
        }
        return $lists;
    }

    /**
     * Counting number of objects in response
     *
     * @param array $data
     *
     * @return int|null
     */
    private static function countResults($data)
    {
        if (!is_array($data))
            self::$dataCount = $data;
        else
            self::$dataCount = count((array)$data);
        return self::$dataCount;
    }

    /**
     * @return mixed
     */
    public static function getRequestCount()
    {
        return self::$requestCount;
    }

    /**
     * @param mixed $requestCount
     */
    public static function setRequestCount($requestCount)
    {
        self::$requestCount = $requestCount;
    }

    /**
     * @return \DateTime
     */
    private static function getLastRequestDate()
    {
        return self::$requestDate;
    }

    /**
     * @param \DateTime $dateTime
     */
    private static function setLastRequestDate(\DateTime $dateTime = null)
    {
        if (self::$requestCount == 0) {
            if (!$dateTime)
                $dateTime = new \DateTime();
            self::$requestDate = $dateTime;
        }
    }

    /**
     * @param string $format
     *
     * @return null|string
     */
    private static function formatLastRequestDate($format = "Y-m-d H:i:s")
    {
        if (self::$requestDate)
            return self::$requestDate->format($format);
        return null;
    }

    private static function getRequestDelay()
    {
        if (!self::$requestDate) {
            return 0;
        }
        $now = new \DateTime();
        $lastRequestDate = self::getLastRequestDate();
        $diff = $now->diff($lastRequestDate)->s;
        if ($diff < self::MAX_REQUEST_INTERVAL) {
            return self::MAX_REQUEST_INTERVAL - $diff;
        }
        return self::MAX_REQUEST_INTERVAL;
    }

    private static function incrementRequestCount()
    {
        if (!self::$requestCount) {
            self::$requestCount = 0;
            self::setLastRequestDate(new \DateTime());
        }
        if (self::$requestCount <= self::MAX_REQUESTS_NUMBER) {
            self::$requestCount++;

            return self::$requestCount;
        } elseif (self::$requestCount > self::MAX_REQUESTS_NUMBER) {
            self::$requestCount = 0;
            return self::$requestCount;
        }
    }
}