<?php
namespace TestApi\Helpers;

use Aura\Cli\CliFactory;
use Aura\Cli\Stdio\Formatter;
use Aura\Cli\Stdio\Handle;
use Aura\Cli\Stdio;

class CliHelper
{
    /**
     * @var Stdio
     */
    private static $stdio;

    /**
     * @var \Aura\Cli\Context
     */
    private static $cliContext;

    /**
     * @var array
     */
    private static $requiredParameters = [
        'path:'
    ];

    /**
     * CliHelper constructor.
     */
    public function __construct()
    {
        $cli = new CliFactory();
        $formatter = new Formatter();
        $stdInHandle = new Handle("php://stdin", 'r');
        $stdOutHandle = new Handle("php://stdout", 'w');
        $stdErrHandle = new Handle("php://stderr", 'w');
        self::$stdio = new Stdio($stdInHandle, $stdOutHandle, $stdErrHandle, $formatter);
        self::$cliContext = $cli->newContext($GLOBALS);
    }

    private static function initialize()
    {
        $cli = new CliFactory();
        $formatter = new Formatter();
        $stdInHandle = new Handle("php://stdin", 'r');
        $stdOutHandle = new Handle("php://stdout", 'w');
        $stdErrHandle = new Handle("php://stderr", 'w');
        self::$stdio = new Stdio($stdInHandle, $stdOutHandle, $stdErrHandle, $formatter);
        self::$cliContext = $cli->newContext($GLOBALS);
    }

    /**
     * @param string $message
     */
    public static function errln($message)
    {
        self::initialize();
        if(is_string($message))
            self::$stdio->errln($message);
        else
        {
            try{
                self::$stdio->errln($message->getMessage());
            }
            catch (\Exception $e)
            {
                self::errln($e->getMessage());
                exit();
            }
        }
    }

    /**
     * @param string $message
     */
    public static function outln($message){
        self::initialize();
        self::$stdio->outln($message);
    }

    /**
     * @return string
     */
    public static function inln()
    {
        self::initialize();
        return self::$stdio->inln();
    }

    /**
     * @param array $messages
     */
    public static function multipleErrln($messages = [])
    {
        self::initialize();
        if(is_array($messages) && count($messages))
        {
            foreach ($messages as $message)
            {
                self::errln($message);
            }
            return;
        }
        self::errln("Wrong argument type for method ".__METHOD__." of class ".__CLASS__."");
    }

    /**
     * @param array $messages
     */
    public static function multipleOutln($messages = [])
    {
        self::initialize();
        if(is_array($messages) && count($messages))
        {
            foreach ($messages as $message)
            {
                self::outln($message);
            }
            return;
        }
        self::errln("Wrong argument type for method ".__METHOD__." of class ".__CLASS__."");
    }

    /**
     * Method allow you to get single parameter from CLI context;
     * @param $key
     *
     * @return mixed|null
     */
    public static function getCliParameter($key)
    {
        self::initialize();
        $cliContextParameters = self::$cliContext->getopt(self::$requiredParameters);
        if($cliContextParameters->hasErrors())
        {
            $cliErrors = $cliContextParameters->getErrors();
            self::multipleErrln($cliErrors);
            exit();
        }
        if($parameterValue = $cliContextParameters->get($key)) {
            return $parameterValue;
        }
        return null;
    }

    /**
     * Method for receiving an array of needed CLI context parameters
     * @param array $keys
     *
     * @return array
     */
    public static function getCliParameters(array $keys)
    {
        self::initialize();
        $result = [];
        foreach ($keys as $key)
        {
            $result[$key] = self::getCliParameter($key);
        }
        return $result;
    }

    public static function getLimitValue($name, $default = 10, $max = 100)
    {
        CliHelper::outln("Enter limit for data count in request for " . $name . " (default: " . $default . "):");
        $limit = CliHelper::inln();
        while ($limit > $max)
        {
            CliHelper::outln("Data limit can't be more then " . $max . ". Try again:");
            $limit = CliHelper::inln();
        }
        $limit = (int) $limit ? (int) $limit : $default;
        return $limit;
    }
}