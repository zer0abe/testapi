<?php
namespace TestApi\Helpers;

use KHerGe\JSON\JSON;

class JSONHelper
{
    protected static $json;

    public function __construct()
    {
        self::$json = new JSON();
    }

    public static function __callStatic($name, $parameters)
    {
        $json = new JSON();
        if(method_exists($json, $name))
        {
            if(count($parameters) == 1)
                return $json->$name($parameters[0]);
            return $json->$name($parameters);
        }
        return null;
    }
}