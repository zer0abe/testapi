<?php

namespace TestApi\Helpers;


class CamelCaseHelper
{
    public static function camelToUnderscore($input)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    public static function underscoreToCamel( $string, $first_char_caps = false)
    {
        if( $first_char_caps == true )
        {
            $string[0] = strtoupper($string[0]);
        }
        return preg_replace_callback('/_([a-z])/',
            create_function('$c', 'return strtoupper($c[1]);'),
            $string);
    }
}