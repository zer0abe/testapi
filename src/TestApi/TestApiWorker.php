<?php
namespace TestApi;

use TestApi\Entity\ApiUser;
use TestApi\Entity\Campaign;
use TestApi\Entity\Group;
use TestApi\Entity\Ad;
use TestApi\Entity\Phrase;
use TestApi\Entity\AdStat;
use TestApi\Entity\PhraseStat;
use TestApi\Entity\Core\CoreEntity;
use TestApi\Helpers\CamelCaseHelper;
use TestApi\Helpers\RequestHelper;
use TestApi\PdoWrapper;
use TestApi\Config;
use TestApi\Helpers\DatabaseHelper;
use TestApi\Helpers\JSONHelper;
use TestApi\Helpers\CliHelper;

class TestApiWorker
{
    /**
     * Little PDO wrapper;
     * @var PdoWrapper
     */
    protected $db;

    /**
     * @var \TestApi\Config
     */
    protected $cfg;

    /**
     * test-api url
     * @var string
     */
    private $url;

    /**
     * Date and time of last request
     * @var \DateTime
     */
    private $lastRequestDate;

    private $requestCount;

    private $campaigns;

    private $groups;

    private $adsKeyList;

    private $phrasesKeyList;

    public function __construct()
    {
        $this->adsKeyList = [];
        $this->phrasesKeyList = [];
        $this->setConfiguration();
//        $this->setDatabase();
        $this->firstRun();
        $this->url = $this->cfg->apiUrl;
    }

    private function setConfiguration()
    {
        $this->cfg = new Config(CliHelper::getCliParameter('--path'));
        if ($this->cfg->isEmpty()) {
            CliHelper::errln("Configuration is empty. Please, fill it.");
            exit();
        }
    }

    public function firstRun()
    {
        CliHelper::outln("Checking your database scheme.");
        $databaseSettings = $this->cfg->database;
        $stmt = DatabaseHelper::checkDatabaseSchema($databaseSettings->DB_NAME);
        $sqlSchema = file_get_contents(BASE_PATH . DIRECTORY_SEPARATOR . $this->cfg->sqlSchema);
        if (!($stmt instanceof \PDOException)) {
            $stmt = DatabaseHelper::prepareScheme($sqlSchema);
            if ($stmt) {
                CliHelper::outln("New database schema was created for you.");
                CliHelper::outln("It's OK. Continue...");
            } else {
                CliHelper::outln("Your database schema is actual.");
                CliHelper::outln("It's OK. Continue...");
            }
        } else {
            CliHelper::errln("Please, check your database configuration. Process finished with next error:" . PHP_EOL . $stmt->getMessage());
            die();
        }
    }

    /**
     * @return ApiUser
     */
    public function auth()
    {
        if (!$email = $this->cfg->userEmail) {
            CliHelper::outln('Please, enter your email: ');
            $email = CliHelper::inln();
        }
        if (!$plainPassword = $this->cfg->userPass) {
            CliHelper::outln('And now enter your password: ');
            $plainPassword = CliHelper::inln();
            if (empty($plainPassword)) {
                CliHelper::errln("Password can't be empty!");
                die();
            }
        }
        $user = DatabaseHelper::fetchUser($email, $plainPassword);
        $lastRequestDate = $user->getLastRequestDate();
        if($lastRequestDate) {
            $now = new \DateTime();
            $lastRequestDate = \DateTime::createFromFormat("Y-m-d H:i:s", $lastRequestDate);
            $diff = $lastRequestDate->diff($now);
            if($user->getLastRequestSuccess())
            {
                if(!$diff->d)
                {
                    CliHelper::outln("All your data is actual now. Try out later. In about 24 hours later.");
                    exit();
                }
            }
        }

        if (!$user->getToken()) {
            CliHelper::outln("Trying to get first token.");
            $this->getNewToken($user);
        } elseif ($user->getToken()) {
            $diff = $user->checkTokenLifeTime();
            if($diff->d >= 1)
                $this->refreshToken($user);
            elseif ($diff->d >= 7)
                $this->getNewToken($user);
        }
        return $user;
    }

    /**
     * @param ApiUser $user
     */
    public function getNewToken(ApiUser &$user)
    {
        CliHelper::outln("Trying to get new token for ".$user->getEmail());
        $url = $this->url.'/api/authenticate';
        $method = 'POST';
        $options = [
            'email' => $user->getEmail(),
            'password' => $user->getPlainPassword()
        ];
        $response = RequestHelper::makeRequest($this, $user, $url, $method, $options);
        if(isset($response->token))
        {
            $user->setToken($response->token);
            $user->setTokenBirthDate(new \DateTime());
            DatabaseHelper::flushEntity($user);
        }
    }

    public function refreshToken(ApiUser &$user)
    {
        CliHelper::outln("Refreshing token for ".$user->getEmail());
        $method = 'GET';
        $url = $this->url . '/api/token';
        if ($responseBody = RequestHelper::makeRequest($this, $user, $url, $method)) {
            if (isset($responseBody->token)) {
                $user->setToken($responseBody->token);
                DatabaseHelper::flushEntity($user);
            }
        }
    }

    /**
     * @param ApiUser $user
     */
    public function requestCampaigns(ApiUser $user)
    {
        $url = $this->url . '/api/campaigns';
        $method = 'POST';
        CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
        $campaigns = RequestHelper::repeatedRequest($this, $user, $url, $method);
        if ($campaigns) {
            foreach ($campaigns as $campaign) {
                $campaign = new Campaign((array)$campaign);
                DatabaseHelper::flushEntity($campaign);
                $this->addCampaign($campaign);
            }
        }
    }

    /**
     * @param ApiUser $user
     */
    public function requestGroups(ApiUser $user)
    {
        $lists = $this->makeKeysList($this->campaigns, 10);
        $method = 'POST';
        $resultingGroups = [];
        foreach ($lists as $list) {
            $url = $this->url . '/api/campaign/' . $list . '/groups';
            CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
            $groups = RequestHelper::repeatedRequest($this, $user, $url, $method);
            $resultingGroups = array_merge($groups, $resultingGroups);
        }
        foreach ($resultingGroups as $k => $group) {
            $group = new Group((array)$group);
            DatabaseHelper::flushEntity($group);
            $this->addGroup($group);
        }
    }

    /**
     * @param ApiUser $user
     */
    public function requestAds(ApiUser $user)
    {
        $limit = CliHelper::getLimitValue('Ads', 50, 50);

        $lists = $this->makeKeysList($this->campaigns, 10);
        $method = 'POST';
        foreach ($lists as $list)
        {
            $url = $this->url . '/api/campaign/'.$list.'/ads';
            CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
            $adsKeyList = RequestHelper::repeatedRequest($this, $user, $url, $method, $limit, true, '\TestApi\Entity\Ad');
            $this->adsKeyList = array_merge($this->adsKeyList, $adsKeyList);
        }
    }

    /**
     * @param ApiUser $user
     */
    public function requestPhrases(ApiUser $user)
    {
        $limit = CliHelper::getLimitValue('Phrases', 100, 100);

        $lists = $this->makeKeysList($this->groups, 10);
        $method = 'POST';
        foreach ($lists as $list)
        {
            $url = $this->url . '/api/group/'.$list.'/phrases';
            CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
            $phrasesKeyList = RequestHelper::repeatedRequest($this, $user, $url, $method, $limit, true, '\TestApi\Entity\Phrase');
            $this->phrasesKeyList = array_merge($this->phrasesKeyList, $phrasesKeyList);
        }
    }

    public function requestAdStats(ApiUser $user)
    {
        $limit = CliHelper::getLimitValue('Ads stats', 100, 100);

        $method = 'POST';
        foreach ($this->adsKeyList as $list)
        {
            $url = $this->url . '/api/ad/'.$list.'/stat';
            CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
            RequestHelper::repeatedRequest($this, $user, $url, $method, $limit, true, '\TestApi\Entity\AdStat');
        }
    }

    public function requestPhraseStats(ApiUser $user)
    {
        $limit = CliHelper::getLimitValue('Phrases stats', 100, 100);

        $method = 'POST';
        foreach ($this->phrasesKeyList as $list)
        {
            $url = $this->url . '/api/phrase/'.$list.'/stat';
            CliHelper::outln("Requesting " . $url . " using method " . $method . " with user " . $user->getEmail());
            RequestHelper::repeatedRequest($this, $user, $url, $method, $limit, true, '\TestApi\Entity\PhraseStat');
        }
    }

    public function finalStep(ApiUser $user)
    {
        $user->setLastRequestDate(new \DateTime());
        $user->setLastRequestStatus(true);
        DatabaseHelper::flushEntity($user);
    }

    public function setLastRequestDate(\DateTime $dateTime)
    {
        $this->lastRequestDate = $dateTime;
    }

    /**
     * @return \DateTime
     */
    public function getLastRequestDate()
    {
        return $this->lastRequestDate;
    }

    /**
     * @return mixed
     */
    public function getRequestCount()
    {
        return $this->requestCount;
    }

    /**
     * @param mixed $requestCount
     */
    public function setRequestCount($requestCount)
    {
        $this->requestCount = $requestCount;
    }

    /**
     * @return array of storred campaigns
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param Campaign $campaign
     **/
    private function addCampaign(Campaign $campaign)
    {
        $this->campaigns[] = $campaign;
    }

    /**
     * @param Group $group
     */
    private function addGroup(Group $group)
    {
        $this->groups[] = $group;
    }

    private function addAd(Ad $ad)
    {
        $this->ads[] = $ad;
    }

    private function makeKeysList(array $data, $length = 10, $stringOut = true,$key = 'test_api_id')
    {
        $method = 'get' . CamelCaseHelper::underscoreToCamel($key);
        $result = [];
        foreach ($data as $k => $entity) {
            if (!$entity instanceof CoreEntity) {
                CliHelper::errln(__CLASS__ . "::" . __METHOD__ . "()" . " can work only with childes of CoreEntity class.");
                die();
            }
            if (!method_exists($entity, $method)) {
                CliHelper::errln($method . " can't be found in entity");
                die();
            }
            $result[] = $entity->$method();
        }
        $lists = array_chunk($result, $length);
        if ($stringOut)
        {
            foreach ($lists as $k => $list) {
                $lists[$k] = implode(';', $lists[$k]);
            }
        }
        return $lists;
    }



    public function getDb()
    {
        return $this->db;
    }
}