<?php
namespace TestApi;

use TestApi\Helpers\CliHelper;
use TestApi\Config;

/**
 * It's a PDO wrapper class
 */
class PdoWrapper
{
    protected static $dsn;
    protected static $user;
    protected static $pass;
    /**
     * @var \PDO instance of PDO class
     */
    protected static $instance = null;

    /**
     * PdoWrapper constructor.
     *
     * @param string $dsn
     * @param string $user
     * @param string $pass
     */
    public function __construct($dsn, $user, $pass)
    {
        self::$dsn = $dsn;
        self::$user = $user;
        self::$pass = $pass;
    }

    public function __clone() {}

    public static function instance()
    {
        if (self::$instance === null)
        {
            $opt  = array(
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_LAZY,
                \PDO::ATTR_EMULATE_PREPARES   => TRUE,
            );
            if(!self::$dsn)
            {
                $settings = Config::getKey('database');
                self::$dsn = $settings->DB_DRIVER . ':host=' . $settings->DB_HOST . ';dbname=' . $settings->DB_NAME . ';charset=' . $settings->DB_CHAR;
                self::$user = $settings->DB_USER;
                self::$pass = $settings->DB_PASS;
            }
            self::$instance = new \PDO(self::$dsn, self::$user, self::$pass, $opt);
        }
        return self::$instance;
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(array(self::instance(), $method), $args);
    }

    public static function run($sql, $args = [])
    {
        $stmt = self::instance()->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    public static function pdoSet($allowed, &$values, $source = array()) {
        $set = '';
        $values = array();
        if (!$source) $source = &$_POST;
        foreach ($allowed as $field) {
            if (isset($source[$field])) {
                $set.="`".str_replace("`","``",$field)."`". "=:$field, ";
                $values[$field] = $source[$field];
            }
        }
        return substr($set, 0, -2);
    }
}