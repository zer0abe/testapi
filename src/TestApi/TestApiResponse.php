<?php
namespace TestApi;


use Buzz\Message\Response;
use TestApi\Entity\Core\CoreEntity;

class TestApiResponse extends Response
{
    private $entity;

    /**
     *
     * @return CoreEntity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param CoreEntity $entity
     */
    public function setEntity(CoreEntity $entity)
    {
        $this->entity = $entity;
    }
}