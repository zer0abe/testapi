<?php
namespace TestApi;

use KHerGe\JSON\JSON;
use KHerGe\JSON\Exception\Exception as JSONException;
use KHerGe\File\Exception\Exception as FileException;
use TestApi\Helpers\CliHelper;

class Config
{
    protected $configuration;

    protected static $instance;

    public function __construct($configurationPath)
    {
        if(!self::$instance) {
            $json = new JSON();
            try {
                $this->configuration = $json->decodeFile($configurationPath);
                unset($json);
                self::$instance = $this;
            } catch (JSONException $e) {
                CliHelper::errln($e->getMessage() . " ERROR_CODE: " . $e->getCode());
                exit($e->getCode());
            } catch (FileException $e) {
                CliHelper::errln($e->getMessage() . " ERROR_CODE: " . $e->getCode());
                exit($e->getCode());
            }
        }
        else
        {
            $this->configuration = self::$instance->configuration;
        }
    }

    public function __get($key)
    {
        if(isset($this->configuration->$key))
            return $this->configuration->$key;
        CliHelper::errln("Can't get parameter ".$key." from configuration file.");
        return null;
    }

    public function isEmpty()
    {
        return !count($this->configuration);
    }

    public static function getKey($key)
    {
        if(!self::$instance)
        {
            $attribute = CliHelper::getCliParameter('--path');
            self::$instance = new Config($attribute);
        }
        return self::$instance->$key;
    }
}