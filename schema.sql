CREATE TABLE IF NOT EXISTS api_users (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(255) NOT NULL,
  plain_password VARCHAR(255) NOT NULL,
  token TEXT DEFAULT NULL,
  last_request_date DATETIME DEFAULT NULL,
  token_birth_date DATETIME DEFAULT NULL,
  last_request_success BOOL DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS campaigns (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR (255) NOT NULL,
  balance INT NOT NULL,
  user_id INT NOT NULL,
  test_api_id INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (test_api_id),
  FOREIGN KEY campaign_user (user_id)
  REFERENCES api_users (id)
  ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS groups (
  id INT NOT NULL AUTO_INCREMENT,
  campaign_id INT NOT NULL,
  name VARCHAR (255) NOT NULL,
  test_api_id INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (test_api_id),
  FOREIGN KEY group_campaign (campaign_id)
  REFERENCES campaigns (test_api_id)
  ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ads (
  id INT NOT NULL AUTO_INCREMENT,
  ad_group_id INT NOT NULL,
  campaign_id INT NOT NULL,
  title VARCHAR (255) NOT NULL,
  text TEXT NOT NULL,
  href TEXT NOT NULL,
  test_api_id INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (test_api_id),
  FOREIGN KEY ad_campaign (campaign_id)
  REFERENCES campaigns (test_api_id)
  ON DELETE CASCADE
#   FOREIGN KEY ad_group (ad_group_id)
#   REFERENCES groups (test_api_id)
#   ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS phrases (
  id INT NOT NULL AUTO_INCREMENT,
  ad_group_id INT NOT NULL,
  campaign_id INT NOT NULL,
  ad_id INt NOT NULL,
  phrase VARCHAR (255) NOT NULL,
  test_api_id INT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (test_api_id),
  FOREIGN KEY phrase_group (ad_group_id)
  REFERENCES groups (test_api_id)
  ON DELETE CASCADE,
  FOREIGN KEY phrase_campaign (campaign_id)
  REFERENCES campaigns (test_api_id)
  ON DELETE CASCADE
#   FOREIGN KEY phrases_ad (ad_id)
#   REFERENCES ads (test_api_id)
#   ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS ad_stats (
  id INT NOT NULL AUTO_INCREMENT,
  ad_id INT NOT NULL,
  phrase_id INT NOT NULL,
  date DATETIME NOT NULL,
  clicks INT DEFAULT 0,
  shows INT DEFAULT 0,
  amount INT DEFAULT 0,
  ctr INT DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY ad (ad_id)
  REFERENCES ads (test_api_id)
  ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS phrase_stats (
  id INT NOT NULL AUTO_INCREMENT,
  ad_id INT NOT NULL,
  phrase_id INT NOT NULL,
  date DATETIME NOT NULL,
  clicks INT DEFAULT 0,
  shows INT DEFAULT 0,
  amount INT DEFAULT 0,
  ctr INT DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY phrase (phrase_id)
  REFERENCES phrases (test_api_id)
  ON DELETE CASCADE
);
