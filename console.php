<?php
include 'vendor/autoload.php';

define('BASE_PATH', dirname(__FILE__));
$worker = new \TestApi\TestApiWorker();

$user = $worker->auth();
$worker->requestCampaigns($user);
$worker->requestGroups($user);
$worker->requestAds($user);
$worker->requestPhrases($user);
$worker->requestAdStats($user);
$worker->requestPhraseStats($user);
$worker->finalStep($user);